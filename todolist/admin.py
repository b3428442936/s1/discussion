from django.contrib import admin
from .models import ToDoItem, Events

# Register your models here.
admin.site.register(ToDoItem)
admin.site.register(Events)