"""
URL configuration for discussion project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path

# Every django application always starts with the package that has the same name. In order for us to access the routes of a different package (todolist), then we have to import/include it within the 'urls.py' file within the package that has the same name as the project.
urlpatterns = [
    # By doing this, we are going to be able to access the routes
    path('todolist/', include('todolist.urls')),
    path('admin/', admin.site.urls),
]
